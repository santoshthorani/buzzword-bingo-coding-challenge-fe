# 🎲 Buzzword Bingo Coding Challenge 🎲
This task built with React, JavaScript, Material UI, Node.js.

### Overview

Buzzword Bingo shall be played by two or more players participating in the same meeting.
The playing surface consists of 5 by 5 squares. All squares are filled with a word at the beginning of the game. No word appears twice on the board. All squares have white color. All players run their own version of the app therefore each player will see a different board setup.
The player now attentively participates in a meeting. Whenever he hears a word that appears on his playing field, he clicks on this square. The corresponding field changes its colour to blue.
As soon as one player has clicked all the squares in a column, in a row or in one of the two main diagonals, the colour of the squares concerned changes to green.
In this case, the player shall shout "Bingo" loudly into the meeting. He's the winner of the current round and the game ends here.

### Installation
After checking out the code from the repo you can compile and run it like this:

1. Clone this repository from Gitlab
2. Navigatre to the Buzzword Bingo directory in terminal
3. Install the dependencies with:
##
    npm start

4. Start the application with 
##
    npm install


This will launch the application in your default browser at http://localhost:3000/ 

### Technologies Used
* React: https://create-react-app.dev/
* Material UI: https://mui.com/material-ui/getting-started/overview/
     

your browser should start up. If not, please goto

    localhost:3000

You should then see a working game like this:

![](docs/Screenshot1.png)
![](docs/Screenshot2.png)
![](docs/Screenshot3.png)
![](docs/Screenshot4.png)
