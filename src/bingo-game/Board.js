import React from "react";
import Square from "./Square";
import Dictionary from "./Dictionary";
import { Button } from "@mui/material";
class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }
  // Function to get initial game status, useful for restarting game
  getInitialState() {
    return {
      size: 5,
      squares: Array(9).fill(false),
      words: this.initializeWords(25),
      //status of game
      gameOver: false,
      //array to hold indexes of winning squares to change the color to green  
      winningIndexes: Array(5).fill(false),
    };
  }
  // Function to initializes words for game
  initializeWords(totalWords) {
    // Using set to avoid duplicate words
    const uniqueWords = new Set();
    while (uniqueWords.size < totalWords) {
      const randomIndex = Math.floor(Math.random() * Dictionary.length);
      uniqueWords.add(Dictionary[randomIndex]);
    }
    return Array.from(uniqueWords);
  }
  // Function to handle square click
  handleClick(i, row, col) {
    const squares = this.state.squares.slice();
    squares[i] = !squares[i];
    this.setState({ squares: squares }, () => {
      this.checkStatus(i, row, col);
    });
  }

  // Function to check game status
  checkStatus(i, row, col) {
    const { size, squares } = this.state;
    // if row completely clicked
    const isRowCompleted = [...Array(size).keys()].every(
      (colIndex) => squares[row * size + colIndex]
    );
    // if  column completely clicked
    const isColCompleted = [...Array(size).keys()].every(
      (rowIndex) => squares[rowIndex * size + col]
    );
    // if  primary diagonal completely clicked
    const isPrimaryDiaogonalCompleted = [...Array(size).keys()].every(
      (index) => squares[index * (size + 1)]
    );

    // if  secondary diagonal completeled clicked
    const isSecondayDiagonalCompleted = [...Array(size).keys()].every(
      (index) => squares[(index + 1) * (size - 1)]
    );

    const winningIndexes = this.state.winningIndexes.slice();

    const completed =
      isRowCompleted ||
      isColCompleted ||
      isPrimaryDiaogonalCompleted ||
      isSecondayDiagonalCompleted;
    // If any row, column, or diagonal is completed, mark game as over
    if (completed) {
      for (let index = 0; index < size; index++) {
        // finding the index of square which is part of winning row, col or diagonal
        const winIndex = isRowCompleted
          ? row * size + index
          : isColCompleted
          ? index * size + col
          : isPrimaryDiaogonalCompleted
          ? index * (size + 1)
          : (index + 1) * (size - 1);

        winningIndexes[winIndex] = true;
      }

      this.setState({ gameOver: true, winningIndexes: winningIndexes });
    }
  }

  renderSquare(i, row, col) {
    return (
      <Square
        isClicked={this.state.squares[i]}
        isWinningSquare={this.state.winningIndexes[i]}
        onClick={() => this.handleClick(i, row, col)}
        text={this.state.words[i]}
        disabled={this.state.gameOver}
      />
    );
  }
  // Functoin to restart game
  restartGame() {
    this.setState(this.getInitialState());
  }

  render() {
    const size = this.state.size;
    const rows = Array(size)
      .fill(0)
      .map((_, row) => {
        const columns = Array(size)
          .fill(0)
          .map((_, col) => this.renderSquare(row * size + col, row, col));
        return <div className="board-row">{columns}</div>;
      });

    return (
      <div>
        {rows}
        <br />
        <Button
          variant="outlined"
          color="secondary"
          onClick={() => this.restartGame()}
        >
          Restart Game
        </Button>
      </div>
    );
  }
}

export default Board;
