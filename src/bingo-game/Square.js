import React from "react";
import { Button } from "@mui/material";

class Square extends React.Component {
  render() {
    // value is renamed to isClicked, to make it more intuitive
    const { isClicked, isWinningSquare, onClick, text, disabled } = this.props;
    // reusing common button attributes
    const commonBtnAttributes = {
      className: "square",
      onClick: onClick,
      disabled: disabled,
    };
// Checking if square is winning 
    if (isWinningSquare) {
      return (
        <Button
          variant="contained"
          {...commonBtnAttributes}
          sx={{
            "&.Mui-disabled": {
              background:  "green",
              color: "white",
            },
          }}
        >
          {text}
        </Button>
      );
    } else if (isClicked) {
      return (
        <Button variant="contained" {...commonBtnAttributes}>
          {text}
        </Button>
      );
    }
    return (
      <Button {...commonBtnAttributes} variant="outlined">
        {text}
      </Button>
    );
  }
}

export default Square;
